<?php

/*
require_once 'app/vendor/errbit-php/lib/Errbit.php';
Errbit::instance()->configure(array(
    'api_key' => '2eb1f8ebaf4dd3fe76b3a97e98d90c92',
    'host' => '188.226.194.42',
    'port' => 3000,
    'secure' => false
))->start();
*/

$f3=require('lib/base.php');

//if ((float)PCRE_VERSION<7.9)
//	trigger_error('PCRE version is out of date');


$f3->set('ONERROR',
    function($f3) {
    	var_dump($f3->get("ERROR"));
        /*
        Errbit::instance()->notify(
        	new Exception($f3->get("ERROR.text"), $f3->get("ERROR.code"))
        );*/
    }
);


$f3->config('config.ini');
$f3->config('routes.ini');

//load RedBeanPHP
require 'app/vendor/rb.phar';
R::setup($f3->get('DBCONN'), $f3->get('DBUSER'), $f3->get('DBPASS'));
//R::debug(true);

$f3->run();
R::close();
