<?php


class Marker extends Controller{
	

	public function getOwnMarkers($f3){
		$login = $this->check_login();
		if($login['result']){
			$user = $login['data'];

			$markers = R::findAll('marker', 'user_id = ?', array($user->id));
			$this->genResult(array('data' => R::beansToArray($markers)));

		}else{
			$this->genResult(array('code' => $login['code']));
		}

	}

	public function getOwnConnections($f3){
		$login = $this->check_login();
		if($login['result']){
			$user = $login['data'];

			$connections = R::findAll('connection', 'user_id = ?', array($user->id));
			foreach($connections as &$conn){
				$conn->fetchAs('marker')->markera;
				$conn->fetchAs('marker')->markerb;
			}
			$this->genResult(array('data' => R::beansToArray($connections)));

		}else{
			$this->genResult(array('code' => $login['code']));
		}

	}

	public function addMarker($f3){
		$login = $this->check_login();
		if($login['result']){
			$user = $login['data'];
			if(!isset($this->jsondata['lat']) || !isset($this->jsondata['lon'])){
				$this->genResult(array('code' => 400)); //Bad request
			}else{

				$marker = R::dispense('marker');

				$marker->user = $user;
				$marker->lat = $this->jsondata['lat'];
				$marker->lon = $this->jsondata['lon'];

				if(R::store($marker)){
					$this->genResult(array('code' => 200, 'data' => $marker->export())); //OK
				}else{
					$this->genResult(array('code' => 500)); //Error
				}
			}
			
		}else{
			$this->genResult(array('code' => $login['code']));
		}
	}


	public function delMarker($f3){
		//$mid = $f3->get('PARAMS.mid');
		$login = $this->check_login();
		if($login['result'] && $f3->get('PARAMS.mid')){
			$user = $login['data'];
			
			$marker = R::load('marker', $f3->get('PARAMS.mid'));
			if($marker->user_id != $user->id){
				$this->genResult(array('code' => '405')); //Not allowed
			}else{

				R::trash($marker);
				$this->genResult(array('code' => 200));
			}
		}else{
			$this->genResult(array('code' => $login['code']));
		}
	}


	public function connectMarker($f3){
		$login = $this->check_login();
		if($login['result']){
			$user = $login['data'];

			if($f3->get('PARAMS.mid1') && $f3->get('PARAMS.mid2')
					&& 0 < R::count('marker', 'id = ? AND user_id = ?', array($f3->get('PARAMS.mid1'), $user->id))
					&& 0 < R::count('marker', 'id = ? AND user_id = ?', array($f3->get('PARAMS.mid2'), $user->id)) ){

				$connection = R::dispense('connection');
				$connection->user = $user;
				$connection->markera_id = $f3->get('PARAMS.mid1');
				$connection->markerb_id = $f3->get('PARAMS.mid2');

				if(R::store($connection)){
					$this->genResult(array('code' => 200)); //OK
				}else{
					$this->genResult(array('code' => 500)); //Error
				}

			}else{
				$this->genResult(array('code' => 400)); //Bad request
			}
		}else{
			$this->genResult(array('code' => $login['code']));
		}
	}

}