<?php

class Controller{


	protected $f3;

	public function __construct(){
		$this->f3 = \Base::instance();



		$this->f3 = Base::instance();


        //parse_str($this->f3->get('BODY'), $req);
        /*
        This is neccessary for the REST interface, to use PUT and DELETE requests.
        */

        /*if(isset($req['jsondata'])){
            $this->jsondata = json_decode($req['jsondata'], true);
        }else{
            $this->jsondata = null;
        }*/
        $this->jsondata = json_decode($this->f3->get('BODY'), true);
        //var_dump($this->jsondata);

	}

	public function check_login(){
		$this->headers = $this->f3->get('HEADERS');

		if(!isset($this->headers['Uid']) || !isset($this->headers['Authtoken'])){
			return array('code' => 400, 'result' => false);
		}else{
			$user = R::findOne('user', 'id = ? AND token = ?', array($this->headers['Uid'], $this->headers['Authtoken']));

			if($user){
				return array('code' => 200, 'result' => true, 'data' => $user); //OK, return with user data
			}else{
				return array('code' => 401, 'result' => false);	//Unauthorized
			}
		}

	}


	/**
	 * Generate result json.
	 *
	 * @param $params['code'] HTTP return code
	 * @param $params['data'] Data to return
	 * 
	 * @return string
	 */
	protected function genResult($params = array()){
		$default_params = array('code' => 200, 'data' => '');
		$params = array_merge($default_params, $params);

		echo '{ "code": '.$params['code'].', "data": '.json_encode($params['data']).' }';
		$this->f3->status($params['code']);
	}


	protected function genToken($user){
		return sha1($user->id.time().rand().$user->id);
	}

}