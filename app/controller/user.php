<?php

class User extends Controller{
	

	public function profile($f3){
		$login = $this->check_login();
		if($login['result']){

			$this->genResult(array('data' => $login['data']));

		}else{
			$this->genResult(array('code' => $login['code']));
		}

	}


	public function register($f3){
		
		if(isset($this->jsondata['user']) && isset($this->jsondata['email']) && isset($this->jsondata['password']) && isset($this->jsondata['password2'])){

			//check pass and add to db
			if($this->jsondata['password'] != $this->jsondata['password2']){
				$this->genResult(array('code' => 400));
			}else{
				$crypt = \Bcrypt::instance();

				$user = R::dispense('user');
				$user->user = $this->jsondata['user'];
				$user->email = $this->jsondata['email'];
				$user->password = $crypt->hash($this->jsondata['password']);
				$user->token = sha1($this->jsondata['user'].time().rand().rand());
				R::store($user);

				$this->genResult(array('data' => $user->export()));
			}
		}else{
			$this->genResult(array('code' => 400));
		}

	}


	public function login($f3){
		if(isset($this->jsondata['user']) && isset($this->jsondata['password'])){
			$user = R::findOne('user', 'user = ?', array($this->jsondata['user']));

			$crypt = \Bcrypt::instance();

			if($user && $crypt->verify($this->jsondata['password'], $user->getHidden('password'))){
				//OK, generate token!
				$user->token = $this->genToken($user);
				R::store($user);

				$this->genResult(array('data' => $user->export()));

			}else{
				$this->genResult(array('code' => 401));	
			}

		}else{
			$this->genResult(array('code' => 400));
		}
	}


	public function logout($f3){
		$login = $this->check_login();
		if($login['result']){
			$user = $login['data'];

			$user->token = sha1(time().$user->id.rand());
			R::store($user);

			$this->genResult(); //empty result with code 200

		}else{
			$this->genResult(array('code' => $login['code']));
		}
	}
}