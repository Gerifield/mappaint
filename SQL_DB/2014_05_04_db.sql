-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2014. Máj 04. 01:21
-- Szerver verzió: 5.6.16
-- PHP verzió: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `mappaint`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `connection`
--

CREATE TABLE IF NOT EXISTS `connection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `markera_id` int(10) unsigned NOT NULL,
  `markerb_id` int(10) unsigned NOT NULL,
  `connection_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `markera` (`markera_id`),
  KEY `markerb` (`markerb_id`),
  KEY `index_foreignkey_connection_user` (`user_id`),
  KEY `index_foreignkey_connection_marker` (`markera_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- A tábla adatainak kiíratása `connection`
--

INSERT INTO `connection` (`id`, `user_id`, `markera_id`, `markerb_id`, `connection_date`) VALUES
(13, 1, 30, 7, '2014-05-03 22:46:34'),
(16, 1, 4, 7, '2014-05-03 22:49:35'),
(17, 1, 31, 4, '2014-05-03 22:52:23'),
(18, 1, 32, 7, '2014-05-03 22:53:44'),
(19, 1, 41, 4, '2014-05-03 23:09:47'),
(20, 1, 4, 30, '2014-05-03 23:09:49'),
(21, 1, 30, 41, '2014-05-03 23:09:49'),
(22, 1, 40, 30, '2014-05-03 23:09:54');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marker`
--

CREATE TABLE IF NOT EXISTS `marker` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_marker_user` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- A tábla adatainak kiíratása `marker`
--

INSERT INTO `marker` (`id`, `user_id`, `lat`, `lon`, `creation_date`) VALUES
(4, 1, 47.408499809434, 18.951549120247, '2014-04-28 15:19:14'),
(7, 1, 47.406688632651, 18.951257094741, '2014-04-30 13:52:03'),
(13, 1, 47.41011307031, 18.947376608849, '2014-04-30 14:03:24'),
(14, 1, 47.470335355903, 19.023093022406, '2014-04-30 14:03:41'),
(15, 1, 47.470757805983, 19.024042524397, '2014-04-30 14:03:41'),
(16, 1, 47.471682243643, 19.023127891123, '2014-04-30 14:03:41'),
(17, 1, 47.471290623104, 19.024128355086, '2014-04-30 14:03:42'),
(18, 1, 47.470986253968, 19.022023156285, '2014-04-30 14:03:42'),
(19, 1, 47.471519068773, 19.022291041911, '2014-04-30 14:03:45'),
(21, 1, 47.471607908487, 19.023559391499, '2014-04-30 14:03:48'),
(22, 1, 47.470456380039, 19.023767597973, '2014-04-30 14:03:50'),
(23, 1, 47.470712252249, 19.022328257561, '2014-04-30 14:03:59'),
(27, 1, 65.627714250638, 92.339630909264, '2014-04-30 14:20:45'),
(28, 1, -21.956179475715, 20.024651326239, '2014-04-30 14:20:47'),
(30, 1, 47.407731280864, 18.950478583574, '2014-05-03 22:44:18'),
(31, 1, 47.408064606265, 18.952506668866, '2014-05-03 22:49:23'),
(32, 1, 47.40741020641, 18.952414803207, '2014-05-03 22:52:14'),
(40, 1, 47.408745999415, 18.950454443693, '2014-05-03 23:05:34'),
(41, 1, 47.408313974944, 18.950859457254, '2014-05-03 23:09:42');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `email`, `creation_date`, `token`) VALUES
(1, 'test', '$2a$10$43YZACLHE4tSp1BRqGhG3uKfrXD8unTdCwolwZFoLVYt9lWfpLAri', 'gergo254@gmail.com', '2014-04-19 14:50:48', 'bbda8d553f019325a0215bcedce89d1019e7e206');

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `connection`
--
ALTER TABLE `connection`
  ADD CONSTRAINT `connection_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connection_ibfk_2` FOREIGN KEY (`markera_id`) REFERENCES `marker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connection_ibfk_3` FOREIGN KEY (`markerb_id`) REFERENCES `marker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `marker`
--
ALTER TABLE `marker`
  ADD CONSTRAINT `marker_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
